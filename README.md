How many people die or suffer from hunger over time? The research provides information for the following time span - 1864-2019. 

## Data

Sources:
  * https://ourworldindata.org/hunger-and-undernourishment
  * https://ourworldindata.org/famines
  * http://www.fao.org/state-of-food-security-nutrition/en/
  * https://www.worldometers.info/
  * https://data.worldbank.org/indicator/sn.itk.defc.zs?end=2017&start=2000&view=chart


## Preparation

* The famine victims values are approximate values and represent roughly the number of hunger victims for each decade - the middle year of the particular decade has been chosen to represent the approximate value for the decade (10 years time frame)
  * Example: 1864 represents the time period 1860-1869
* Until 2000, each chosen year represents approximately a value for the particular decade, after 2000 the values refer to the particular year. 
* Since the time span is big, there are many missing values. However, this is not an obstacle for drawing conclusions and hunger trends. 

## License

This Data Package is made available under the Public Domain Dedication and License v1.0 whose full text can be found at: http://www.opendatacommons.org/licenses/pddl/1.0/