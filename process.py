import csv
import requests
from bs4 import BeautifulSoup

#request webpage
result = requests.get('https://en.wikipedia.org/wiki/List_of_famines')

#save content in src variable
src = result.content

#soup object
soup = BeautifulSoup(src, 'lxml')

#find the table
stat_table = soup.find_all('table', class_ = 'sortable wikitable')
#resultset --> tag
stat_table = stat_table[0]

#write csv file
with open('faminesnew.csv', 'w', newline='', encoding="utf-8") as f:
    writer = csv.writer(f, delimiter=',', lineterminator='\n')
    for tr in stat_table('tr'):
    	row = [t.get_text(strip=True) for t in tr(['td', 'th'])]
    	writer.writerow(row)
